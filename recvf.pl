#!/usr/bin/perl

# Copyright (c) 2016, Jean-Benoist Leger <jb@leger.tf>
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#     Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use strict;
use warnings;
use IPC::Open2;
use IO::Handle;
autoflush STDOUT 1;
use Time::HiRes qw ( time usleep );

my $dirname='/tmp';

if($#ARGV + 1 < 1)
{
    print STDERR "must specify the client\n";
    exit(1);
}

my $cmd = $ARGV[0];
open2(my $cmdout, my $cmdin, $cmd);

my $told=time();
my $ifps=0;
while(1)
{
    print $cmdin "send\n";

    open(my $b64, "| base64 -d > $dirname/mprov.png");
    my $loop=1;
    while($loop)
    {
        my $read = <$cmdout>;
        if($read =~ m/^---/)
        {
            $loop=0;
        }
        else
        {
            print $b64 $read;
        }
    }
    close($b64);

    system("cat $dirname/mprov.png > $dirname/m.png");
    my $tnew=time();

    $ifps += .1*($tnew-$told-$ifps);
    $told = $tnew;

    printf "ifps = %.02f\tnew image\n", $ifps;
}

