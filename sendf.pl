#!/usr/bin/perl

# Copyright (c) 2016, Jean-Benoist Leger <jb@leger.tf>
# 
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are
# met:
# 
#     Redistributions of source code must retain the above copyright
#     notice, this list of conditions and the following disclaimer.
# 
#     Redistributions in binary form must reproduce the above copyright
#     notice, this list of conditions and the following disclaimer in
#     the documentation and/or other materials provided with the
#     distribution.
# 
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
# A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT
# HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
# SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
# LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE,
# DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY
# THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

use warnings;
use Time::HiRes qw ( time usleep );
use IO::Handle;
autoflush STDOUT 1;

# create dir

open(my $f, "mktemp -d /tmp/sendf_XXXXXX |");
my $dirname=<$f>;
chomp($dirname);
close($f);

my $last = 0;

while(1)
{
    while((time()-$last)<1)
    {
        usleep(100000);
    }
    $last=time();

    # we wait a order from stdin
    while( not (<STDIN> =~ m/^send$/) ) {}

    system("DISPLAY=:0 scrot -z $dirname/m.png");

    system("convert $dirname/m.png +dither -colors 256 -depth 8 $dirname/m2.png");

    open(my $fdb,"base64 $dirname/m2.png |");
    while(<$fdb>) { print; }
    close($fdb);

    print "---\n";
}
